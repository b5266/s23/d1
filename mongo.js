// select a database
use <database name>

/*when creating a new database via the command line, 
the use command can be entered wiht a name of a database that
does not exist. Once a new record is inserted into that database,
the database will be created.*/

// database = filling cabinet 
// collection = drawer
// document/record = folder inside a drawer
// fields = file/folder content

/*e.g document with no sub-documents:
	name: "Zandro",
	age: 33
	occupation: "Student"
	address: {
		street: "123 johnson st.,",
		city: "makati",
		country: "Philippines"
	}
e.g document with sub-documents:
{
	name: "Zandro",
	age: 33
	occupation: "Student"
	address: {
		street: "123 johnson st.,",
		city: "makati",
		country: "Philippines"
	}
}
*/

/*
	Embedded vs. referenced data:

	referenced data:

	Users:
	{
		id: 298
		name: "Zandro"
		age: 33
		Occupation: "Student"
	}

	Orders:
	{
		products: [
				{
					name: "New Pillow",
					price: 300
				}
		],
	}
*/

// Insert One Document(Create)

db.users.insert({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "1234567891",
		email: "janedoe@yahoo.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "None"
})

// if inserting/creating a new document within a colection that does not yet exists,

//Insert to many

db.users.insertMany([
	{
		firstName: "Steven",
		lastName: "Hawking",
		age: 45,
		contact: {
			phone: "123456789",
			email: "stevenhawking@yahoo.com"
		},
		courses: ["PHP", "React", "Python"],
		department: "None"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "123456789",
			email: "neilarmstrong@yahoo.com"
		},
		courses: ["SASS", "React", "Laravel"],
		department: "None"
	}
])

// Finding documents (Read)

// retrieve a list of ALL users
db.users.find()

//find a specific user
db.users.find({firstName: "Steven"})// finds any and all matches
db.users.findOne({firstName: "Steven"})// find only the first match

//find documents with multiple parameters/conditions
db.users.find({lastName: "Armstrong", age: 82})

//Update/Edit documents

//Create a new document to update
db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "1234567891",
		email: "test@yahoo.com"
	},
	courses: [],
	department: "None"
})

//updateOne always needs to be provided two objects. The first object spcifies WHICH document to update.
// - the second 
db.users.updateOne(
	{_id:ObjectId("628ccd7508671b8565a76d02")},
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "123456789",
				email: "billgates@yahoo.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations"
		}
	}
)

//Update Multiple documents
db.users.updateMany(
	{department: "None"},
	{
		$set: {
			department: "HR"
		}
	}
)

//creating a document to delete
db.users.insert({
	firstName: "Test"
})

//Deleting a single document
db.users.deleteOne({
	firstName: "Test"
})

//deleting many records
//Be careful when using deleteMany. If no search criteria are provided, it will delete ALL documents within the given collection

//
db.users.insert({
	firstName: "Bill"
})

db.users.deleteMany({
	firstName: "Bill"
})

